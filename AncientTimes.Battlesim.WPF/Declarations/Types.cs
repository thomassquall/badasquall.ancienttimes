﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.Battlesim.WPF.Declarations
{
    public enum PokemonType
    {
        Fire = 0,
        Water = 1,
        Grass = 2,
        Normal = 3,
        Bug = 4,
        Flying = 5,
        Electric = 6,
        Ground = 7,
        Fairy = 8,
        Dragon = 9,
        Dark = 10,
        Ghost = 11,
        Psychic = 12,
        Rock = 13,
        Ice = 14,
        Iron = 15,
        Fight = 16
    }
}
