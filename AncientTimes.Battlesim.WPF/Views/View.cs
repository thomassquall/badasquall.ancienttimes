﻿using AncientTimes.Battlesim.WPF.Declarations;
using AncientTimes.Battlesim.WPF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AncientTimes.Battlesim.WPF.Views
{
    public class View : ViewBase
    {
        private List<PokemonType> typesList;

        public List<PokemonType> TypesList
        {
            get { return typesList; }
            set { Raise(ref typesList, value); }
        }

        private Pokemon enemy;

        public Pokemon Enemy
        {
            get { return enemy; }
            set { Raise(ref enemy, value); }
        }

        private PokemonType typeOne;

        public PokemonType TypeOne
        {
            get { return typeOne; }
            set { Raise(ref typeOne, value); }
        }

        private PokemonType typeTwo;

        public PokemonType TypeTwo
        {
            get { return typeTwo; }
            set { Raise(ref typeTwo, value); }
        }

        public View()
        {
            TypesList = Enum.GetValues(typeof(PokemonType)).Cast<PokemonType>().ToList();
        }
    }
}