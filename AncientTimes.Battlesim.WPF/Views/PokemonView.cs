﻿using AncientTimes.Battlesim.WPF.Declarations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.Battlesim.WPF.Views
{
    public class PokemonView : ViewBase
    {
        private PokemonType typeOne;

        public PokemonType TypeOne
        {
            get { return typeOne; }
            set { Raise(ref typeOne, value); }
        }

        private PokemonType typeTwo;

        public PokemonType TypeTwo
        {
            get { return typeTwo; }
            set { Raise(ref typeTwo, value); }
        }
    }
}