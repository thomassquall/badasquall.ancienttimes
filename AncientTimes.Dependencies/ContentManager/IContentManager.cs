﻿namespace AncientTimes.Dependencies.ContentManager
{
    public interface IContentManager
    {
        /// <summary>
        /// Extracts content from embedded resources, optionally filtering with param filter (defined as folder.subfolder.file.ext).
        /// </summary>
        void ExtractContent(string filter = "");

        /// <summary>
        /// Delete extracted content, optionally filtering with param filter (defined as folder.subfolder.file.ext).
        /// </summary>
        void DeleteContent();
    }
}
