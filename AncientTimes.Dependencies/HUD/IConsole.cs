﻿using System;

namespace AncientTimes.Dependencies.HUD
{
    #region Usings

    using BSGameFramework.GameBase;

    #endregion Usings

    public interface IConsole
    {
        #region Events

        /// <summary>
        /// Occurs when [message complete].
        /// </summary>
        event Action MessageComplete;

        #endregion Events

        #region Methods

        /// <summary>
        /// Sets the console in the stage.
        /// </summary>
        /// <param name="stage">The stage.</param>
        /// <returns>True if the action is successfull.</returns>
        bool SetInStage(StageBase stage);

        /// <summary>
        /// Writes the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Write(string message);

        #endregion Methods
    }
}