﻿using BSGameFramework.GameBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.Dependencies.MapCore
{
    public interface IMap
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name of the map.
        /// </summary>
        /// <value>
        /// The name of the map.
        /// </value>
        string MapName { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        int Width { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        int Height { get; set; }

        /// <summary>
        /// Gets or sets the width of the tile.
        /// </summary>
        /// <value>
        /// The width of the tile.
        /// </value>
        int TileWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the tile.
        /// </summary>
        /// <value>
        /// The height of the tile.
        /// </value>
        int TileHeight { get; set; }

        /// <summary>
        /// Gets or sets the music.
        /// </summary>
        /// <value>
        /// The music.
        /// </value>
        string Music { get; set; }

        /// <summary>
        /// Gets or sets the tilesets.
        /// </summary>
        /// <value>
        /// The tilesets.
        /// </value>
        List<ITileset> Tilesets { get; set; }

        /// <summary>
        /// Gets or sets the layers.
        /// </summary>
        /// <value>
        /// The layers.
        /// </value>
        List<ILayer> Layers { get; set; }

        /// <summary>
        /// Gets or sets the tiles.
        /// </summary>
        /// <value>
        /// The tiles.
        /// </value>
        List<ITileData> Tiles { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Initilizes itself in the specified stage.
        /// </summary>
        /// <param name="stage">The stage.</param>
        void Initilize(StageBase stage);

        #endregion Methods
    }
}