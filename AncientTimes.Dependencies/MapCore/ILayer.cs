﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.Dependencies.MapCore
{
    public interface ILayer
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; set; }

        /// <summary>
        /// The tiles.
        /// </summary>
        List<ITileData> Tiles { get; set; }

        #endregion Properties
    }
}