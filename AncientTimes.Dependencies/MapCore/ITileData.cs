﻿using BSGameFramework.GameBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.Dependencies.MapCore
{
    public interface ITileData
    {
        #region Properties

        /// <summary>
        /// Gets or sets the gid.
        /// </summary>
        /// <value>
        /// The gid.
        /// </value>
        int GID { get; set; }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        AnimatedSprite2D Image { get; set; }

        /// <summary>
        /// Gets or sets the properties.
        /// </summary>
        /// <value>
        /// The properties.
        /// </value>
        Dictionary<string, string> Properties { get; set; }

        #endregion Properties
    }
}