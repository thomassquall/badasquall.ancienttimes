﻿using System;

namespace AncientTimes.Dependencies.Logger
{
    public interface ILogger
    {
        void Write(Exception ex);
        void Write(string message);
    }
}