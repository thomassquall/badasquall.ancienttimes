﻿using BSGameFramework.GameBase;

namespace AncientTimes.Dependencies.Core
{
    public interface IStartingStage
    {
        StageBase Get();
    }
}
