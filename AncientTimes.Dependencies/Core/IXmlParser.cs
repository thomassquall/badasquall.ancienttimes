﻿using System.Collections.Generic;
namespace AncientTimes.Dependencies.Core
{
    public interface IXmlParser
    {
        /// <summary>
        /// Parses the specified file path.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="element">Optional element to be included in the parse.</param>
        Dictionary<string, object> Parse(string filePath, object element = null);
    }
}