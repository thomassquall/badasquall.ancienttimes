﻿namespace AncientTimes.Dependencies.Bootstrapper
{
    public interface IGame
    {
        void Run();
    }
}