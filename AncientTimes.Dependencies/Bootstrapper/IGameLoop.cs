﻿namespace AncientTimes.Dependencies.Bootstrapper
{
    #region Usings

    using System.Collections.Generic;

    #endregion Usings

    /// <summary>
    /// The IGameLoop interface.
    /// </summary>
    public interface IGameLoop
    {
        #region Methods

        /// <summary>
        /// Runs the game loop.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        void Run(IList<string> args);

        #endregion Methods
    }
}
