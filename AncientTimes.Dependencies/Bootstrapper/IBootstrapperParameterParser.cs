﻿namespace AncientTimes.Dependencies.Bootstrapper
{
    #region Usings

    using System.Collections.Generic;

    #endregion Usings

    /// <summary>
    /// The IBootstrapperParameterParser interface.
    /// </summary>
    public interface IBootstrapperParameterParser
    {
        #region Methods

        /// <summary>
        /// Parses the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        void Parse(IList<string> args);

        #endregion Methods
    }
}