﻿using BSGameFramework.Input;

namespace AncientTimes.Core
{
    #region Usings

    using AncientTimes.Dependencies.MapCore;
    using BSGameFramework;
    using BSGameFramework.CoreLayer;
    using BSGameFramework.GameBase;
    using BSGameFramework.Graphics;
    using Dependencies.Core;
    using Dependencies.HUD;
    using LuaInterface;
    using System;
    using System.Collections.Generic;

    #endregion Usings

    internal class AncientTimesStage : StageBase
    {
        #region Properties

        private readonly string className;
        protected readonly Lua Lua;
        private readonly AncientTimes.Dependencies.Core.IXmlParser parser;
        private readonly IConsole console;
        private bool noLogic;
        private Label debugging;
        private int cycleCounts;
        private int millisecondsPassed;

        #endregion Properties

        #region Constructor

        public AncientTimesStage(string className, AncientTimes.Dependencies.Core.IXmlParser parser, IConsole console)
        {
            this.parser = parser;
            this.console = console;
            this.className = className;
            Lua = new Lua();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Loads the content.
        /// </summary>
        public override void LoadContent()
        {
            debugging = new Label()
            {
                Font = FilesManager.LoadSpriteFont("Verdana", 50),
                Position = new Vector2(20, 20),
                ZIndex = -10,
                Color = Color.Red
            };

            debugging.Text = "FPS: 0";

            AddComponent("debugging", debugging);

            var components = parser.Parse(@"Content/Stages/" + className + @"/Init.xml", this);
            if (components != null)
                foreach (var obj in components)
                {
                    AddComponent(obj.Key, (IComponent)obj.Value);
                    if (obj.Value is IMap) (obj.Value as IMap).Initilize(this);
                }

            console.SetInStage(this);

            try
            {
                Lua.DoFile(@"Content/Stages/" + className + @"/Logic.lua");
            }
            catch (LuaScriptException ex)
            {
                noLogic = true;
            }

            if (!noLogic)
            {
                Lua["Console"] = console;
                Lua["Parser"] = parser;
                Lua["this"] = this;
                Lua.GetFunction("LoadContent").Call();
            }

            base.LoadContent();
        }

        /// <summary>
        /// Updates the stage.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            millisecondsPassed += gameTime.ElapsedGameTime.Milliseconds;
            cycleCounts++;

            if (millisecondsPassed >= 1000)
            {
                millisecondsPassed = 0;

                debugging.Text = "FPS: " + cycleCounts.ToString();
                cycleCounts = 0;
            }

            if (!noLogic) Lua.GetFunction("Update").Call(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// Backs from stack.
        /// </summary>
        public override void BackFromStack()
        {
            console.SetInStage(this);

            base.BackFromStack();
        }

        #endregion Methods
    }
}