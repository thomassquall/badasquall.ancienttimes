﻿namespace AncientTimes.Core
{
    #region Usings

    using AncientTimes.Dependencies.Core;
    using BSGameFramework.CoreLayer;
    using BSGameFramework.GameBase;
    using BSGameFramework.Graphics;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    #endregion Usings

    public class StageInitParser : IXmlParser
    {
        #region Properties

        private readonly AncientTimes.Dependencies.MapCore.IXmlParser mapParser;

        #endregion Properties

        #region Constructor

        public StageInitParser(AncientTimes.Dependencies.MapCore.IXmlParser mapParser)
        {
            this.mapParser = mapParser;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Parses the specified file path.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="element">Optional element to be included in the parse.</param>
        public Dictionary<string, object> Parse(string filePath, object element = null)
        {
            var objects = new Dictionary<string, object>();
            var document = XDocument.Load(filePath);
            if (document == null) throw new System.Exception("The init file does not exist");

            var map = (from m in document.Descendants("Map")
                       select new
                       {
                           Key = m.Attribute("Key").Value,
                           Path = m.Attribute("Path").Value
                       }).FirstOrDefault();

            if (map != null)
                foreach (var m in ParseForMap(map.Path, map.Key))
                {
                    objects.Add(m.Key, m.Value);
                }

            foreach (var obj in ParseForAnimatedSprite2D(document))
            {
                objects.Add(obj.Key, obj.Value);
            }

            return objects;
        }

        private Dictionary<string, object> ParseForAnimatedSprite2D(XDocument document)
        {
            var objects = new Dictionary<string, object>();

            var animatedSprites2D = from sprite in document.Descendants("AnimatedSprite2D")
                                    select new
                                    {
                                        Path = sprite.Attribute("Path").Value,
                                        Key = sprite.Attribute("Key").Value,
                                        Rows = int.Parse(sprite.Attribute("Rows").Value),
                                        Columns = int.Parse(sprite.Attribute("Columns").Value),
                                        Position = (from position in sprite.Descendants("Position")
                                                    select new
                                                    {
                                                        X = float.Parse(position.Attribute("X").Value),
                                                        Y = float.Parse(position.Attribute("Y").Value)
                                                    }).FirstOrDefault(),
                                        FirstFrame = (from firstFrame in sprite.Descendants("FirstFrame")
                                                      select new Rectangle()
                                                      {
                                                          X = int.Parse(firstFrame.Attribute("X").Value),
                                                          Y = int.Parse(firstFrame.Attribute("Y").Value),
                                                          Width = int.Parse(firstFrame.Attribute("Width").Value),
                                                          Height = int.Parse(firstFrame.Attribute("Height").Value)
                                                      }).FirstOrDefault(),
                                        AutoAnimation = (from animation in sprite.Descendants("AutoAnimation")
                                                         select new
                                                         {
                                                             FirstColumn = int.Parse(animation.Attribute("FirstColumn").Value),
                                                             ColumnCount = int.Parse(animation.Attribute("ColumnCount").Value),
                                                             SpriteSheetRow = int.Parse(animation.Attribute("SpriteSheetRow").Value),
                                                             VideoCycles = int.Parse(animation.Attribute("VideoCycles").Value)
                                                         }).FirstOrDefault(),
                                        Animations = from animation in sprite.Descendants("Animation")
                                                     select new
                                                     {
                                                         Name = animation.Attribute("Name").Value,
                                                         FirstColumn = int.Parse(animation.Attribute("FirstColumn").Value),
                                                         ColumnCount = int.Parse(animation.Attribute("ColumnCount").Value),
                                                         SpriteSheetRow = int.Parse(animation.Attribute("SpriteSheetRow").Value),
                                                         VideoCycles = int.Parse(animation.Attribute("VideoCycles").Value)
                                                     }
                                    };

            foreach (var sprite in animatedSprites2D)
            {
                var sprite2D = new AnimatedSprite2D(FilesManager.LoadTexture2D(sprite.Path, false))
                {
                    RowCount = sprite.Rows,
                    ColumnCount = sprite.Columns,
                    Position = new BSGameFramework.Graphics.Vector2(sprite.Position != null ? sprite.Position.X : 0,
                        sprite.Position != null ? sprite.Position.Y : 0),
                    PortionArea = sprite.FirstFrame
                };

                if (sprite.AutoAnimation != null)
                    sprite2D.AutoAnimation = new Animation()
                    {
                        FirstColumn = sprite.AutoAnimation.FirstColumn,
                        ColumnCount = sprite.AutoAnimation.ColumnCount,
                        SpriteSheetRow = sprite.AutoAnimation.SpriteSheetRow,
                        VideoCycles = sprite.AutoAnimation.VideoCycles
                    };

                foreach (var animation in sprite.Animations)
                {
                    sprite2D.AddAnimation(animation.Name, new Animation()
                        {
                            FirstColumn = animation.FirstColumn,
                            ColumnCount = animation.ColumnCount,
                            SpriteSheetRow = animation.SpriteSheetRow,
                            VideoCycles = animation.VideoCycles,
                            SkipFirstFrame = true,
                            LoopCount = -1
                        });
                }

                objects.Add(sprite.Key, sprite2D);
            }

            return objects;
        }

        private Dictionary<string, object> ParseForMap(string mapPath, string mapKey)
        {
            var objects = new Dictionary<string, object>();

            objects = mapParser.Parse(mapPath, mapKey);

            return objects;
        }

        #endregion Methods
    }
}