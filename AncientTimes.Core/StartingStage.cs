﻿using AncientTimes.Dependencies.HUD;

namespace AncientTimes.Core
{
    #region Usings

    using BSGameFramework.GameBase;
    using Dependencies.Core;

    #endregion Usings

    public class StartingStage : IStartingStage
    {
        #region Properties

        private readonly IXmlParser parser;
        private readonly IConsole console;

        #endregion Properties

        #region Constructor

        public StartingStage(IXmlParser parser, IConsole console)
        {
            this.parser = parser;
            this.console = console;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the first stage of the game.
        /// </summary>
        /// <returns>The stage.</returns>
        public StageBase Get()
        {
            return new AncientTimesStage("StageMap", parser, console);
        }

        #endregion Methods
    }
}