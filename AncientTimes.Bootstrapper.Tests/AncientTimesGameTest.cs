﻿using AncientTimes.Dependencies.Bootstrapper;
using AncientTimes.Dependencies.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AncientTimes.Bootstrapper.Tests
{
    [TestClass]
    public class AncientTimesGameTest
    {
        [TestMethod]
        public void Instance()
        {
            var game = DependencyFactory.Resolve<IGame>();

            Assert.IsNotNull(game);
        }
    }
}