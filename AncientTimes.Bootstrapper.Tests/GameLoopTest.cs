﻿using System;
using AncientTimes.Bootstrapper.Fakes;
using AncientTimes.Dependencies.Bootstrapper;
using AncientTimes.Dependencies.Bootstrapper.Fakes;
using AncientTimes.Dependencies.ContentManager;
using AncientTimes.Dependencies.ContentManager.Fakes;
using AncientTimes.Dependencies.Logger;
using AncientTimes.Dependencies.Logger.Fakes;
using Microsoft.Practices.Unity;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AncientTimes.Bootstrapper.Tests
{
    [TestClass]
    public class GameLoopTest
    {
        [TestMethod]
        public void RunWithNullArgument()
        {
            var container = new UnityContainer();
            container.RegisterType<IBootstrapperParameterParser, StubIBootstrapperParameterParser>();
            container.RegisterType<ILogger, StubILogger>();
            container.RegisterType<IGame, StubIGame>();
            container.RegisterType<IContentManager, StubIContentManager>();

            container.Resolve<GameLoop>().Run(null);
        }

        [TestMethod]
        public void RunWithoutNullArgument()
        {
            var container = new UnityContainer();
            container.RegisterType<IBootstrapperParameterParser, StubIBootstrapperParameterParser>();
            container.RegisterType<ILogger, StubILogger>();
            container.RegisterType<IGame, StubIGame>();
            container.RegisterType<IContentManager, StubIContentManager>();

            container.Resolve<GameLoop>().Run(new[] { "ciao" } );
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void RunWithException()
        {
            var container = new UnityContainer();
            container.RegisterType<IBootstrapperParameterParser, StubIBootstrapperParameterParser>();
            container.RegisterType<ILogger, StubILogger>();

            using (ShimsContext.Create())
            {
                var game = new ShimAncientTimesGame()
                {
                    AncientTimesDependenciesBootstrapperIGameRun = () =>
                    {
                        throw new Exception();
                    }
                };

                container.RegisterInstance(typeof(IGame), game.Instance);
            }

            container.RegisterType<IContentManager, StubIContentManager>();
            
            container.Resolve<GameLoop>().Run(null);
        }
    }
}  