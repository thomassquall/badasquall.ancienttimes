﻿using AncientTimes.Dependencies.Bootstrapper;
using AncientTimes.Dependencies.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AncientTimes.Bootstrapper.Tests
{
    /// <summary>
    /// Summary description for BootstrapperParameterParserTest
    /// </summary>
    [TestClass]
    public class BootstrapperParameterParserTest
    {
        [TestMethod]
        public void ParseWithNullArgument()
        {
            DependencyFactory.Resolve<IBootstrapperParameterParser>().Parse(null);
        }

        [TestMethod]
        public void ParseWithoutNullArgument()
        {
            DependencyFactory.Resolve<IBootstrapperParameterParser>().Parse(new [] { "ciao" });
        }
    }
}