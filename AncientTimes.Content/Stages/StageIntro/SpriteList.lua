﻿local SpriteList = { Sprite }

setmetatable(SpriteList, { __index = SpriteList.Sprite })

function SpriteList.new()
    return setmetatable({}, getmetatable(self))
end

return SpriteList