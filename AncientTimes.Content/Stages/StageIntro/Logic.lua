luanet.load_assembly("BSGame")
luanet.load_assembly("System")
Vector2 = luanet.import_type("BSGameFramework.Graphics.Vector2")
Keyboard = luanet.import_type("BSGameFramework.Input.Keyboard")
Keys = luanet.import_type("BSGameFramework.Input.Keys")

SysConsole = luanet.import_type("System.Console")

timeElapsedAnimating = 0
kernethFrame = 0
levitationSpan = 0.5
levitationMovementsCount = 0
kernethList = {}
kernethList.Count = 0
startAnimation = false

state = 0

stateFunctions = { StartExplanation, Animation, Explanation, SexChoice }

function kernethList.Add(sprite)
	kernethList[kernethList.Count] = sprite
	kernethList.Count = kernethList.Count + 1
end

function LoadContent()
	for i = 0, 39, 1 do
		sprite = this.GetComponent("Kerneth"..i)
		kernethList.Add(sprite)
	end

	kerneth = kernethList[0]
	kerneth.IsVisible = true
	kerneth.Position = Vector2((1920 - kerneth.Width) / 2, 100)

	Console.MessageComplete:Add(IncrementState)
end

function Update(gameTime)
	SwitchState(gameTime) 
	
	Levitate()
end

function Levitate()
	kerneth.Position = Vector2(kerneth.Position.X, kerneth.Position.Y + levitationSpan)
	levitationMovementsCount = levitationMovementsCount + 1

	if levitationMovementsCount == 30 then
		levitationMovementsCount = 0
		levitationSpan = levitationSpan * (-1)
	end
end

function Animation(gameTime)
	timeElapsedAnimating = timeElapsedAnimating + gameTime.ElapsedGameTime.Milliseconds;

	if (timeElapsedAnimating >= 50) then
		timeElapsedAnimating = 0;
	else
		return;
	end

	kernethFrame = kernethFrame + 1
	kerneth.Texture = kernethList[kernethFrame].Texture;

	if (kernethFrame == kernethList.Count - 1) then
		IncrementState()
	end
end

function StartExplanation(gameTime)
	Console:Write("Ben arrivato!")
	Console:Write("Benvenuto in questo mondo popolato dai Pok�mon!")
	Console:Write("Io sono Kerneth ma tutti mi chiamano 'vecchio saggio' perch� ho\n\ndedicato tutta la mia vita allo studio e al rispetto dei Pok�mon!")
	Console:Write("Forse ti sarai chiesto che cosa siano i Pok�mon di cui tanto parlo:\n\ntoh, eccone uno, quando si dice la coincidenza.")
end

function Explanation(gameTime)
	Console:Write("Su questo pianeta viviamo in simbiosi con creature che chiamiamo\n\nPok�mon!\n\nSono davvero amici fedeli sai?")
	Console:Write("A volte persino preziosi alleati!")
	Console:Write("Nonostante ci� non sappiamo ancora tutto su queste meraviglie della\n\nnatura.")
	Console:Write("Per questo mi sono ritirato tanti anni fa qui lontano da tutti per vivere\n\nancora di pi� a contatto con loro!")
	Console:Write("Vorrai perdonarmi ma ultimamente la mia vista � calata....\n\nperci� dimmi:")
end

function MaleFemaleEnter()

end

function IncrementState()
	state = state + 1
end

function SwitchState(gameTime)
	func = stateFunctions[state]
	
	if func ~= nil then
		func(gameTime)
	end
end