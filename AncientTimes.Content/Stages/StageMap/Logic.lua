luanet.load_assembly("Core")
luanet.load_assembly("BSGame")

Keyboard = luanet.import_type("BSGameFramework.Input.Keyboard")
Keys = luanet.import_type("BSGameFramework.Input.Keys")

AncientTimesStage = luanet.import_type("AncientTimes.Core.AncientTimesStage")
StageTransactionArgs = luanet.import_type("StageTransactionArgs")
TransactionTo = luanet.import_type("TransactionTo")

function LoadContent()
	leon = this:GetComponent("LeonOW")
end

function Update(gameTime)
	if Keyboard.IsKeyDownSync(Keys.Up) then
		leon:StartAnimation("MoveUp", true)
	end
	if Keyboard.IsKeyDownSync(Keys.Down) then
		leon:StartAnimation("MoveDown", true)
	end
	if Keyboard.IsKeyDownSync(Keys.Left) then
		leon:StartAnimation("MoveLeft", true)
	end
	if Keyboard.IsKeyDownSync(Keys.Right) then
		leon:StartAnimation("MoveRight", true)
	end
end