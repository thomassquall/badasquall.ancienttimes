﻿using BSGameFramework.CoreLayer;
using BSGameFramework.Graphics;
using BSGameFramework.Input;
using System.Collections.Generic;
using System.Linq;

namespace AncientTimes.HUD
{
    #region Usings

    using BSGameFramework;
    using BSGameFramework.GameBase;
    using Dependencies.HUD;
    using System;

    #endregion Usings

    public class Console : IConsole, IUpdatableComponent
    {
        #region Properties

        private static int blinkMilliseconds;
        private static int blinkTimes;
        private static Sprite2D consoleBackground;
        private static Sprite2D nxtMsgTriangle;
        private static Label message;
        private static List<string> text;
        private static StageBase prevStage;
        private static Console instance;
        private const int MaximumWritingSpeed = 5;

        private static event Action messageComplete;

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public static Console Instance
        {
            get { return instance ?? (instance = new Console()); }
        }

        /// <summary>
        /// Occurs when [message complete].
        /// </summary>
        public event Action MessageComplete
        {
            add { messageComplete = messageComplete == null ? new Action(value) : value; }
            remove { if (message == null) return; messageComplete = value; }
        }

        #endregion Properties

        #region Constructor

        private Console()
        {
            text = new List<string>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Sets the console in the stage.
        /// </summary>
        /// <param name="stage">The stage.</param>
        /// <returns>True if the action is successfull.</returns>
        public bool SetInStage(StageBase stage)
        {
            if (stage == null) return false;

            message = message ?? new Label()
            {
                Font = FilesManager.LoadSpriteFont("Verdana", 50),
                Position = new Vector2(80, 780)
            };

            consoleBackground = consoleBackground ?? new Sprite2D(FilesManager.LoadTexture2D(@"Content\Console\background.png", true))
            {
                Position = new Vector2(0, 735),
                IsVisible = false
            };

            nxtMsgTriangle = nxtMsgTriangle ?? new Sprite2D(FilesManager.LoadTexture2D(@"Content\Console\nxtmsgtriangle.png", true))
            {
                Position = new Vector2(1820, 1000),
                IsVisible = false
            };

            if (prevStage != null)
            {
                prevStage.RemoveComponent("consoleBackground");
                prevStage.RemoveComponent("nxtMsgTriangle");
                prevStage.RemoveComponent("message");
                prevStage.RemoveComponent("console");
            }

            prevStage = stage;

            prevStage.AddComponent("consoleBackground", consoleBackground);
            prevStage.AddComponent("nxtMsgTriangle", nxtMsgTriangle);
            prevStage.AddComponent("message", message);
            prevStage.AddComponent("console", this);

            ClearText();

            return true;
        }

        /// <summary>
        /// Writes the specified message.
        /// </summary>
        /// <param name="text">The message.</param>
        public void Write(string text)
        {
            Console.text.Add(text);
            consoleBackground.IsVisible = true;
        }

        /// <summary>
        /// Clears the message.
        /// </summary>
        private static void ClearText()
        {
            message.Text = "";
        }

        /// <summary>
        /// Updates the Console.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public void Update(GameTime gameTime)
        {
            if (text.Count != 0 && text.First() != message.Text) { TypeMessage(); return; }

            if (text.Count == 0) return;

            if (!TriangleBlink(gameTime)) return;

            if (!Keyboard.IsKeyDownSync(Keys.Enter)) return;

            blinkMilliseconds = 0;
            blinkTimes = 0;
            text.Remove(text.First());

            if (text.Count == 0 && messageComplete != null) messageComplete();

            message.Text = "";
            if (text.Count == 0) consoleBackground.IsVisible = false;
            nxtMsgTriangle.IsVisible = false;
        }

        /// <summary>
        /// Types the message.
        /// </summary>
        private static void TypeMessage()
        {
            var numberOfLetters = 1;

            if (Keyboard.IsKeyDown(Keys.Enter)) numberOfLetters = MaximumWritingSpeed;

            numberOfLetters = numberOfLetters == 1 || text.First().Length - message.Text.Length < MaximumWritingSpeed ? 1 : MaximumWritingSpeed;

            message.Text = text.First().Substring(0, message.Text.Length + numberOfLetters);
        }

        /// <summary>
        /// Makes the triangle blinking.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        /// <returns></returns>
        private static bool TriangleBlink(GameTime gameTime)
        {
            blinkMilliseconds += gameTime.ElapsedGameTime.Milliseconds;

            if (blinkMilliseconds < 300) return blinkTimes >= 1;
            blinkMilliseconds = 0;
            ++blinkTimes;
            nxtMsgTriangle.IsVisible = !nxtMsgTriangle.IsVisible;

            return false;
        }

        #endregion Methods
    }
}