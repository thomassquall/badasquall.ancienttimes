﻿using AncientTimes.Dependencies.Configuration;

namespace AncientTimes.Main
{
    #region Usings

    using System;
    using Dependencies.Bootstrapper;

    #endregion Usings

    /// <summary>
    /// The class containing the app entry point.
    /// </summary>
    class EntryPoint
    {
        #region Main

        [STAThread]
        static void Main(string[] args)
        {
            DependencyFactory.Resolve<IGameLoop>().Run(args);
        }

        #endregion Main
    }
}