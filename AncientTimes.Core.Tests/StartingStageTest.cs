﻿using System;
using AncientTimes.Dependencies.Configuration;
using AncientTimes.Dependencies.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AncientTimes.Core.Tests
{
    [TestClass]
    public class StartingStageTest
    {
        [TestMethod]
        public void GetReturnsNotNullInstance()
        {
            var instance = DependencyFactory.Resolve<IStartingStage>().Get();

            Assert.IsNotNull(instance);
        }
    }
}
