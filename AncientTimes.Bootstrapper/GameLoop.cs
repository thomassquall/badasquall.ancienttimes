using AncientTimes.Dependencies.ContentManager;
using AncientTimes.Dependencies.Core;

namespace AncientTimes.Bootstrapper
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using Dependencies.Bootstrapper;
    using Dependencies.Logger;

    #endregion Usings

    /// <summary>
    /// IGameLoop implementation.
    /// </summary>
    public class GameLoop : IGameLoop
    {
        #region Properties

        private readonly IBootstrapperParameterParser bootstrapperParameterParser;
        private readonly ILogger logger;
        private readonly IGame game;
        private readonly IContentManager contentManager;

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLoop"/> class.
        /// </summary>
        /// <param name="bootstrapperParameterParser">The bootstrapper parameter parser.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="game">The game.</param>
        /// <param name="contentManager">The content manager.</param>
        public GameLoop(IBootstrapperParameterParser bootstrapperParameterParser, ILogger logger, IGame game, IContentManager contentManager)
        {
            this.bootstrapperParameterParser = bootstrapperParameterParser;
            this.logger = logger;
            this.game = game;
            this.contentManager = contentManager;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Runs the game loop.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        public void Run(IList<string> args)
        {
            try
            {
                bootstrapperParameterParser.Parse(args);
                contentManager.ExtractContent();

                game.Run();
            }
            catch (Exception ex)
            {
                logger.Write(ex);
                throw;
            }
            finally
            {
                contentManager.DeleteContent();
            }
        }

        #endregion Methods
    }
}