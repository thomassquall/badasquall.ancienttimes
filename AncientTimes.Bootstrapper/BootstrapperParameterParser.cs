﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AncientTimes.Dependencies.Bootstrapper;

namespace AncientTimes.Bootstrapper
{
    public class BootstrapperParameterParser : IBootstrapperParameterParser
    {
        public void Parse(IList<string> args)
        {
            if (args == null) return;

            if (IsContainingDebugOption(args) && !Debugger.IsAttached)
                Debugger.Launch();
        }

        private static bool IsContainingDebugOption(IList<string> args)
        {
            return args.Any() && args[0] == "--debug";
        }
    }
}