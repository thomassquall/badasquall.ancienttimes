﻿namespace AncientTimes.Bootstrapper
{
    #region Usings

    using Dependencies.Bootstrapper;
    using Dependencies.Core;

    #endregion Usings

    /// <summary>
    /// The Ancient Times game.
    /// </summary>
    public class AncientTimesGame : BSGameFramework.Game, IGame
    {
        #region Properties

        private readonly IStartingStage startingStage;

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AncientTimesGame"/> class.
        /// </summary>
        /// <param name="startingStage">The starting stage.</param>
        public AncientTimesGame(IStartingStage startingStage)
        {
            this.startingStage = startingStage;

            WindowWidth = 1920;
            WindowHeight = 1080;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes the game.
        /// </summary>
        protected override void Initialize()
        {
            StartingStage = startingStage.Get();

            base.Initialize();
        }

        #endregion Methods
    }
}