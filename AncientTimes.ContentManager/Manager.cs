﻿namespace AncientTimes.ContentManager
{
    #region Usings

    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Dependencies.ContentManager;

    #endregion Usings

    public class Manager : IContentManager
    {
        #region Methods

        /// <summary>
        /// Extracts content from embedded resources, optionally filtering with param filter (defined as folder.subfolder.file.ext).
        /// </summary>
        /// <param name="filter"></param>
        public void ExtractContent(string filter = "")
        {
            if (!Directory.Exists("Content"))
                Directory.CreateDirectory("Content");

            var resources = Assembly.LoadFrom(@"Content.dll").GetManifestResourceNames();

            CreateContentAndResources(resources, "Content", filter);
        }

        /// <summary>
        /// Delete extracted content, optionally filtering with param filter (defined as folder.subfolder.file.ext).
        /// </summary>
        public void DeleteContent()
        {
            if (!Directory.Exists("Content")) return;

            Directory.Delete("Content", true);
        }

        /// <summary>
        /// Creates the content and resources.
        /// </summary>
        /// <param name="resources">The resources.</param>
        /// <param name="destinationDirectory">The destination directory.</param>
        /// <param name="filter">The filter.</param>
        private static void CreateContentAndResources(IEnumerable<string> resources, string destinationDirectory, string filter)
        {
            foreach (var resource in resources.Where(r => r.Contains(filter)))
            {
                var tokenizedResource = resource.Split('.');
                var tokenCount = tokenizedResource.Count();
                var lastPath = @destinationDirectory;
                
                for (var i = 1; i < tokenCount - 2; i++)
                {
                    lastPath = @lastPath + @"\" + @tokenizedResource[i];
                    if (!Directory.Exists(lastPath))
                    {
                        Directory.CreateDirectory(lastPath);
                    }
                }

                var filename = tokenizedResource[tokenCount - 2] + "." + tokenizedResource[tokenCount - 1];

                var readStream = Assembly.LoadFrom(@"Content.dll").GetManifestResourceStream(resource);
                if (readStream == null) continue;
                var writeStream = new FileStream(@lastPath + @"\" + @filename, FileMode.Create);
                readStream.CopyTo(writeStream);
                readStream.Close();
                writeStream.Close();

                File.SetAttributes(@lastPath + @"\" + @filename, FileAttributes.Normal);
            }
        }

        #endregion Methods
    }
}