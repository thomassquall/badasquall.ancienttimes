﻿using AncientTimes.Dependencies.MapCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.MapCore
{
    public class Layer : ILayer
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// The tiles.
        /// </summary>
        public List<ITileData> Tiles { get; set; }

        #endregion Properties

        #region Constructor

        public Layer()
        {
            Tiles = new List<ITileData>();
        }

        #endregion Constructor
    }
}