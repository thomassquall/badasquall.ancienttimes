﻿using AncientTimes.Dependencies;
using AncientTimes.Dependencies.MapCore;
using BSGameFramework.CoreLayer;
using BSGameFramework.GameBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace AncientTimes.MapCore
{
    public class MapParser : IXmlParser
    {
        #region Properties

        private readonly IMap map;
        private readonly IFactory factory;
        private TileProperties tilesProperties;
        private List<int> firstGIDs;

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MapParser"/> class.
        /// </summary>
        /// <param name="map">The map.</param>
        public MapParser(IMap map, IFactory factory)
        {
            this.map = map;
            this.factory = factory;
            tilesProperties = new TileProperties();
            firstGIDs = new List<int>();
        }

        #endregion Constructor

        #region Methods

        public Dictionary<string, object> Parse(string filePath, object element = null)
        {
            var objects = new Dictionary<string, object>();
            var key = element as string;
            if (string.IsNullOrWhiteSpace(element as string)) throw new System.Exception("Map key for " + filePath + " cannot be null!");
            var document = XDocument.Load(filePath);
            if (document == null) throw new System.Exception("The init file does not exist");

            TilesetParser.ParseForTilesets(document, tilesProperties, firstGIDs, factory).ForEach(tileset => { map.Tilesets.Add((ITileset)tileset); });

            LayerParser.ParseForLayers(document, factory).ForEach(layer => { map.Layers.Add((ILayer)layer); AddPropertiesToTiles(((ILayer)layer).Tiles); });

            objects.Add(key, map);

            return objects;
        }

        private void AddPropertiesToTiles(List<ITileData> tiles)
        {
            tiles.ForEach(tile =>
            {
                var firstGID = firstGIDs.Last();

                for (var f = 0; f < firstGIDs.Count - 1; f++)
                {
                    if (tile.GID >= firstGIDs[f] && tile.GID < firstGIDs[f])
                    {
                        firstGID = firstGIDs[f];
                        break;
                    }
                }

                if (tilesProperties.ContainsKey(tile.GID - firstGID)) tilesProperties[tile.GID - firstGID].ForEach(tp => tile.Properties.Add(tp.Name, tp.Value));
            });
        }

        #endregion Methods
    }
}