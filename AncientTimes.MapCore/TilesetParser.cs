﻿using AncientTimes.Dependencies;
using AncientTimes.Dependencies.MapCore;
using BSGameFramework.CoreLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace AncientTimes.MapCore
{
    internal static class TilesetParser
    {
        /// <summary>
        /// Parses for tilesets.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        public static List<object> ParseForTilesets(XDocument document, TileProperties tilesProperties, List<int> firstGIDs, IFactory factory)
        {
            var tilesets = from tileset in document.Descendants("tileset")
                           select new
                           {
                               Name = tileset.Attribute("name").Value,
                               Width = int.Parse(tileset.Attribute("tilewidth").Value),
                               Height = int.Parse(tileset.Attribute("tileheight").Value),
                               FirstGID = int.Parse(tileset.Attribute("firstgid").Value),
                               TilesProperties = from tile in tileset.Descendants("tile")
                                                 select new
                                                 {
                                                     Id = int.Parse(tile.Attribute("id").Value),
                                                     Properties = from property in tile.Descendants("property")
                                                                  select new AncientTimes.MapCore.TileProperties.NameValue
                                                                  {
                                                                      Name = property.Attribute("name").Value,
                                                                      Value = property.Attribute("value").Value
                                                                  }
                                                 }
                           };

            var realTilesets = new List<ITileset>();

            foreach (var t in tilesets)
            {
                ITileset tileset = factory.ResolveItem<ITileset>();
                tileset.Name = t.Name;
                tileset.Texture = FilesManager.LoadTexture2D(@"Content/Map/" + t.Name + ".png", false);
                tileset.Width = t.Width;
                tileset.Height = t.Height;
                tileset.FirstGID = t.FirstGID;
                firstGIDs.Add(t.FirstGID);

                foreach (var tp in t.TilesProperties)
                {
                    var nameValues = new List<AncientTimes.MapCore.TileProperties.NameValue>();

                    foreach (var p in tp.Properties) nameValues.Add(new AncientTimes.MapCore.TileProperties.NameValue() { Name = p.Name, Value = p.Value });

                    tilesProperties.Add(tp.Id, nameValues);
                }

                realTilesets.Add(tileset);
            }

            return new List<object>(realTilesets);
        }
    }
}