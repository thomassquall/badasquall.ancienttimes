﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.MapCore
{
    internal class TileProperties : Dictionary<int, List<AncientTimes.MapCore.TileProperties.NameValue>>
    {
        public struct NameValue
        {
            public string Name { get; set; }

            public string Value { get; set; }
        };
    };
}