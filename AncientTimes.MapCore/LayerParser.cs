﻿using AncientTimes.Dependencies;
using AncientTimes.Dependencies.MapCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace AncientTimes.MapCore
{
    internal static class LayerParser
    {
        /// <summary>
        /// Parses for layers.
        /// </summary>
        /// <param name="document">The document.</param>
        public static List<object> ParseForLayers(XDocument document, IFactory factory)
        {
            var layers = from layer in document.Descendants("layer")
                         select new
                         {
                             Name = layer.Attribute("name").Value,
                             Tiles = from tile in layer.Descendants("tile")
                                     select new
                                     {
                                         GID = int.Parse(tile.Attribute("gid").Value),
                                         Properties = from property in tile.Descendants("property")
                                                      select new
                                                      {
                                                          Key = property.Attribute("name").Value,
                                                          Value = property.Attribute("value").Value
                                                      }
                                     }
                         };

            var realLayers = new List<ILayer>();

            foreach (var l in layers)
            {
                var layer = factory.ResolveItem<ILayer>();
                layer.Name = l.Name;

                foreach (var t in l.Tiles)
                {
                    layer.Tiles.Add(factory.ResolveItem<ITileData>());
                    layer.Tiles.Last().GID = t.GID;
                }

                realLayers.Add(layer);
            }

            return new List<object>(realLayers);
        }
    }
}