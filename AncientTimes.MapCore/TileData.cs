﻿using AncientTimes.Dependencies.MapCore;
using BSGameFramework.GameBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.MapCore
{
    public class TileData : ITileData
    {
        #region Properties

        /// <summary>
        /// Gets or sets the gid.
        /// </summary>
        /// <value>
        /// The gid.
        /// </value>
        public int GID { get; set; }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        public AnimatedSprite2D Image { get; set; }

        /// <summary>
        /// Gets or sets the properties.
        /// </summary>
        /// <value>
        /// The properties.
        /// </value>
        public Dictionary<string, string> Properties { get; set; }

        #endregion Properties

        #region Constructor

        public TileData()
        {
            Properties = new Dictionary<string, string>();
        }

        #endregion Constructor
    }
}