﻿#region Usings

using AncientTimes.Dependencies.MapCore;
using BSGameFramework.GameBase;
using BSGameFramework.Graphics;
using BSGameFramework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion Usings

namespace AncientTimes.MapCore
{
    public class Map : IMap, IUpdatableComponent
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name of the map.
        /// </summary>
        /// <value>
        /// The name of the map.
        /// </value>
        public string MapName { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the width of the tile.
        /// </summary>
        /// <value>
        /// The width of the tile.
        /// </value>
        public int TileWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the tile.
        /// </summary>
        /// <value>
        /// The height of the tile.
        /// </value>
        public int TileHeight { get; set; }

        /// <summary>
        /// Gets or sets the music.
        /// </summary>
        /// <value>
        /// The music.
        /// </value>
        public string Music { get; set; }

        /// <summary>
        /// Gets or sets the tilesets.
        /// </summary>
        /// <value>
        /// The tilesets.
        /// </value>
        public List<ITileset> Tilesets { get; set; }

        /// <summary>
        /// Gets or sets the layers.
        /// </summary>
        /// <value>
        /// The layers.
        /// </value>
        public List<ILayer> Layers { get; set; }

        /// <summary>
        /// Gets or sets the tiles.
        /// </summary>
        /// <value>
        /// The tiles.
        /// </value>
        public List<ITileData> Tiles { get; set; }

        #endregion Properties

        #region Constructor

        public Map()
        {
            Tilesets = new List<ITileset>();
            Layers = new List<ILayer>();
            Tiles = new List<ITileData>();
        }

        #endregion Constructor

        #region Methods

        public void Initilize(StageBase stage)
        {
            foreach (var layer in Layers)
            {
                var posX = 0;
                var posY = 0;

                foreach (var tile in layer.Tiles)
                {
                    for (int tilesetCount = 0; tilesetCount < Tilesets.Count; tilesetCount++)
                    {
                        var tileset = Tilesets[tilesetCount];

                        if (tile.GID >= tileset.FirstGID && tilesetCount + 1 < Tilesets.Count && tile.GID < Tilesets[tilesetCount + 1].FirstGID)
                        {
                            SetTile(tile, tileset, stage, ref posX, ref posY);
                            break;
                        }
                        else
                        {
                            if (tile.GID >= tileset.FirstGID && tilesetCount + 1 >= Tilesets.Count)
                            {
                                SetTile(tile, tileset, stage, ref posX, ref posY);
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void SetTile(ITileData tile, ITileset tileset, StageBase stage, ref int posX, ref int posY)
        {
            var tilePositionOnImage = tile.GID - tileset.FirstGID;
            tile.Image = new AnimatedSprite2D(tileset.Texture);
            tile.Image.PortionArea = new BSGameFramework.Graphics.Rectangle(tilePositionOnImage % (tile.Image.Width / tileset.Width)
                * tileset.Width,
                tilePositionOnImage / (tile.Image.Width / tileset.Width) * tileset.Height, 64, 64);
            tile.Image.Position = new BSGameFramework.Graphics.Vector2(-500 + posX * 64, posY * 64);

            stage.AddComponent("map: " + MapName + " tileset: " + tileset.Name + " tile: " + (++posX + tile.Image.Width / tileset.Width * posY), tile.Image);

            if (posX >= tile.Image.Width / tileset.Width)
            {
                posX = 0;
                posY++;
            }
        }

        public void Update(BSGameFramework.GameTime gameTime)
        {
            if (Keyboard.IsKeyDown(Keys.Up)) MoveMap(Direction.Up);
            if (Keyboard.IsKeyDown(Keys.Down)) MoveMap(Direction.Down);
            if (Keyboard.IsKeyDown(Keys.Left)) MoveMap(Direction.Left);
            if (Keyboard.IsKeyDown(Keys.Right)) MoveMap(Direction.Right);
        }

        private void MoveMap(Direction direction)
        {
            int movementSpeed = 10;

            foreach (var layer in Layers)
            {
                foreach (var tile in layer.Tiles)
                {
                    switch (direction)
                    {
                        case Direction.Up:
                            tile.Image.Position = new Vector2(tile.Image.Position.X, tile.Image.Position.Y + movementSpeed);
                            break;

                        case Direction.Down:
                            tile.Image.Position = new Vector2(tile.Image.Position.X, tile.Image.Position.Y - movementSpeed);
                            break;

                        case Direction.Left:
                            tile.Image.Position = new Vector2(tile.Image.Position.X + movementSpeed, tile.Image.Position.Y);
                            break;

                        case Direction.Right:
                            tile.Image.Position = new Vector2(tile.Image.Position.X - movementSpeed, tile.Image.Position.Y);
                            break;
                    }
                }
            }
        }

        #endregion Methods
    }

    internal enum Direction
    {
        Up = 0,
        Down = 1,
        Left = 2,
        Right = 3
    }
}