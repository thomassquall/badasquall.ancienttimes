﻿namespace AncientTimes.Dependencies.Configuration
{
    #region Usings

    using Microsoft.Practices.Unity;

    #endregion Usings

    /// <summary>
    /// Class used by dependency injection to obtain information about the framework elements.
    /// </summary>
    public class DependencyFactory : IFactory
    {
        #region Properties

        /// <summary>
        /// Public reference to the unity container which will
        /// allow the ability to register instances or take
        /// other actions on the container.
        /// </summary>
        /// <value>
        /// The container.
        /// </value>
        internal static UnityContainer Container { get; private set; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Static constructor for DependencyFactory which will
        /// initialize the unity container.
        /// </summary>
        static DependencyFactory()
        {
            Container = new UnityContainer();
            var filler = new UnityContainerFiller(Container);

            filler.Fill();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyFactory"/> class.
        /// </summary>
        public DependencyFactory()
        { }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Resolves the type parameter T to an instance of the appropriate type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        public T ResolveItem<T>()
        {
            return Resolve<T>();
        }

        #endregion Methods
    }
}