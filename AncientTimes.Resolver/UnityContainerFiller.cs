﻿using AncientTimes.Dependencies.HUD;
using AncientTimes.HUD;

namespace AncientTimes.Dependencies.Configuration
{
    #region Usings

    using AncientTimes.Bootstrapper;
    using AncientTimes.ContentManager;
    using AncientTimes.Core;
    using AncientTimes.Dependencies.MapCore;
    using AncientTimes.Logger;
    using AncientTimes.MapCore;
    using Bootstrapper;
    using ContentManager;
    using Core;
    using Logger;
    using Microsoft.Practices.Unity;

    #endregion Usings

    /// <summary>
    /// Helper class used to fill an UnityContainer.
    /// </summary>
    internal class UnityContainerFiller
    {
        #region Properties

        private readonly UnityContainer container;

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityContainerFiller"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public UnityContainerFiller(UnityContainer container)
        {
            this.container = container;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Fills the container.
        /// </summary>
        public void Fill()
        {
            container.RegisterType<IGameLoop, GameLoop>();
            container.RegisterType<IGame, AncientTimesGame>();
            container.RegisterType<IStartingStage, StartingStage>();
            container.RegisterType<AncientTimes.Dependencies.Core.IXmlParser, StageInitParser>();
            container.RegisterType<IBootstrapperParameterParser, BootstrapperParameterParser>();
            container.RegisterType<ILogger, TextLogger>();
            container.RegisterType<IContentManager, Manager>();
            container.RegisterType<IMap, Map>();
            container.RegisterType<ILayer, Layer>();
            container.RegisterType<ITileset, Tileset>();
            container.RegisterType<ITileData, TileData>();
            container.RegisterType<AncientTimes.Dependencies.MapCore.IXmlParser, MapParser>();
            container.RegisterInstance<IConsole>(Console.Instance);
            container.RegisterType<IFactory, DependencyFactory>();
        }

        #endregion Methods
    }
}