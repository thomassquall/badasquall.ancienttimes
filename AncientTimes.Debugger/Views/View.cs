﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AncientTimes.Debugger.WPF.Views
{
    public class View : ViewBase
    {
        private bool processHasExited;

        public bool ProcessHasExited
        {
            get { return processHasExited; }
        }
    }
}
