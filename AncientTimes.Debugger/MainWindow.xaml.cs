﻿using System.Diagnostics;
using System.Windows;

namespace AncientTimes.Debugger.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.Diagnostics.Process process;

        public MainWindow()
        {
            InitializeComponent();

            launcher.Click += launcher_Click;
        }

        private void launcher_Click(object sender, RoutedEventArgs e)
        {
            var info = new ProcessStartInfo { FileName = "PAT.exe" };
            info.Arguments = arguments.Text;
            process = Process.Start(info);
        }
    }
}
