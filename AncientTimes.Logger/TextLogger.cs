﻿namespace AncientTimes.Logger
{
    #region Usings

    using System;
    using System.Diagnostics;
    using Dependencies.Logger;

    #endregion Usings

    public class TextLogger : ILogger
    {
        #region Methods

        public void Write(Exception ex)
        {
            Write(ConvertToString(ex));
        }

        public void Write(string message)
        {
            using (var file = new System.IO.StreamWriter(@"log.txt"))
            {
                file.Write(message);
            }
        }

        private static string ConvertToString(Exception ex)
        {
            return ex != null ? ex.Message + ConvertToString(ex.InnerException) : "";
        }

        #endregion Methods
    }
}